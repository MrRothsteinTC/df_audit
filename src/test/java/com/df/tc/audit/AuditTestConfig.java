/**
 * 
 */
package com.df.tc.audit;

import org.apache.accumulo.core.client.BatchWriterConfig;
import org.apache.accumulo.core.client.Connector;
import org.apache.accumulo.core.client.mock.MockInstance;
import org.apache.accumulo.core.client.security.tokens.PasswordToken;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * @author Steve Chernyak
 *
 */
@Configuration
@Import({ AuditConfig.class })
public class AuditTestConfig extends AuditConfig {

	public @Bean
	Connector connector() throws Exception {
		return new MockInstance().getConnector("testUser",
				new PasswordToken("testPassword".getBytes()));
	}

	public @Bean
	BatchWriterConfig batchWriterConfig() throws Exception {
		return new BatchWriterConfig();
	}

}
