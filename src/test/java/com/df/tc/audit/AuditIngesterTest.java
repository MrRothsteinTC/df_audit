package com.df.tc.audit;

import java.io.ByteArrayInputStream;
import java.util.Date;
import java.util.Map.Entry;

import org.apache.accumulo.core.client.Connector;
import org.apache.accumulo.core.client.Scanner;
import org.apache.accumulo.core.data.Key;
import org.apache.accumulo.core.data.Range;
import org.apache.accumulo.core.data.Value;
import org.apache.accumulo.core.security.Authorizations;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

public class AuditIngesterTest {

	private AnnotationConfigApplicationContext context;

	@Before
	public void setUp() {
		try {
			context = new AnnotationConfigApplicationContext(
					AuditTestConfig.class);
		} catch (Exception e) {
			Assert.fail(e.getMessage());
			e.printStackTrace();
		}
	}

	@Test
	public void testBasicIngest() throws Exception {
		Connector connector = context.getBean(Connector.class);
		connector.tableOperations().create(AuditIngester.DEFAULT_TABLE_NAME);
		AuditIngester ingester = context.getBean(AuditIngester.class);
		Audit audit = new Audit(new Date(), "0001", new ByteArrayInputStream(
				"Simple content.".getBytes()));
		ingester.ingest(audit);
		Scanner scanner = connector.createScanner(
				AuditIngester.DEFAULT_TABLE_NAME, new Authorizations());

		scanner.setRange(new Range());
		for (Entry<Key, Value> entry : scanner) {
			System.out.println("Key: " + entry.getKey() + ", Value: "
					+ entry.getValue());
		}
	}

	@After
	public void tearDown() {
		context.close();
	}

}
