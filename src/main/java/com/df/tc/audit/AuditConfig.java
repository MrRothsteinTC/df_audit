/**
 * 
 */
package com.df.tc.audit;

import org.apache.accumulo.core.client.BatchWriterConfig;
import org.apache.accumulo.core.client.Connector;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.df.tc.audit.id.UUIDAuditRowIdGenerator;

/**
 * @author Steve Chernyak
 * 
 */
@Configuration
public class AuditConfig {

	private @Autowired
	Connector connector;

	private @Autowired
	BatchWriterConfig batchWriterConfig;

	public @Bean
	AuditIngester auditIngester() throws Exception {
		return new DefautAuditIngester(batchWriterConfig, connector,
				rowIdGenerator());
	}
	
	public @Bean
	AuditRowIdGenerator rowIdGenerator() throws Exception {
		return new UUIDAuditRowIdGenerator();
	}
}
