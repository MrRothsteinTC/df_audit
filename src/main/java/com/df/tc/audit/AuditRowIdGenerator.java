/**
 * 
 */
package com.df.tc.audit;

/**
 * @author Steve Chernyak
 *
 */
public interface AuditRowIdGenerator {
	
	/**
	 * Generate an id.
	 * @param audit
	 * @return
	 */
	public String generate(Audit audit);

}
