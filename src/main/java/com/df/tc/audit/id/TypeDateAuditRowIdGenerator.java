package com.df.tc.audit.id;

import java.text.SimpleDateFormat;

import com.df.tc.audit.Audit;
import com.df.tc.audit.AuditRowIdGenerator;

public class TypeDateAuditRowIdGenerator implements AuditRowIdGenerator {
	
	private static SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");

	@Override
	public String generate(Audit audit) {
		return audit.getType() + "-" + DATE_FORMAT.format(audit.getDate());
	}

}
