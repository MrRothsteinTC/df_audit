/**
 * 
 */
package com.df.tc.audit.id;

import java.util.UUID;

import com.df.tc.audit.Audit;
import com.df.tc.audit.AuditRowIdGenerator;

/**
 * @author Steve Chernyak
 *
 */
public class UUIDAuditRowIdGenerator implements AuditRowIdGenerator {

	/* (non-Javadoc)
	 * @see com.df.tc.audit.AuditRowIdGenerator#generate(com.df.tc.audit.Audit)
	 */
	@Override
	public String generate(Audit audit) {
		return UUID.randomUUID().toString();
	}

}
