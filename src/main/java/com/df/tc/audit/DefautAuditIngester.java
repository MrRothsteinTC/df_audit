/**
 * 
 */
package com.df.tc.audit;

import java.io.IOException;
import java.util.Set;

import org.apache.accumulo.core.client.BatchWriter;
import org.apache.accumulo.core.client.BatchWriterConfig;
import org.apache.accumulo.core.client.Connector;
import org.apache.accumulo.core.client.MutationsRejectedException;
import org.apache.accumulo.core.data.Mutation;
import org.apache.accumulo.core.data.Value;
import org.apache.commons.io.IOUtils;
import org.apache.hadoop.io.Text;

/**
 * @author Steve Chernyak
 * 
 */
public class DefautAuditIngester implements AuditIngester {

	private AuditRowIdGenerator idGenerator;
	private BatchWriterConfig batchWriterConfig;
	private Connector connector;
	private String tableName;

	DefautAuditIngester(BatchWriterConfig batchWriterConfig,
			Connector connector, AuditRowIdGenerator idGenerator) {
		this(batchWriterConfig, connector, idGenerator, DEFAULT_TABLE_NAME);
	}

	DefautAuditIngester(BatchWriterConfig batchWriterConfig,
			Connector connector,
			AuditRowIdGenerator idGenerator, String tableName) {
		this.idGenerator = idGenerator;
		this.batchWriterConfig = batchWriterConfig;
		this.connector = connector;
		this.tableName = tableName;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.df.tc.audit.AuditIngester#ingest(com.df.tc.audit.Audit)
	 */
	public void ingest(Audit audit) throws AuditException {
		try {
			BatchWriter batchWriter = connector.createBatchWriter(tableName,
					batchWriterConfig);
			doIngest(batchWriter, audit);
			batchWriter.close();
		} catch (Exception e) {
			throw new AuditException("Ingest failed.", e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.df.tc.audit.AuditIngester#ingest(java.util.Set)
	 */
	public void ingest(Set<Audit> audits) throws AuditException {
		try {
			BatchWriter batchWriter = connector.createBatchWriter(tableName,
					batchWriterConfig);
			for (Audit audit : audits) {
				doIngest(batchWriter, audit);
			}
			batchWriter.close();
		} catch (Exception e) {
			throw new AuditException("Ingest failed.", e);
		}
	}

	protected void doIngest(BatchWriter batchWriter, Audit audit)
			throws IOException, MutationsRejectedException {
		Mutation mutation = new Mutation(createRowId(audit));
		mutation.put("type", "", audit.getType());
		mutation.put("timestamp", "", String.valueOf(audit.getDate().getTime()));
		mutation.put(new Text("content"), new Text(""),
				new Value(IOUtils.toByteArray(audit.getContent())));
		batchWriter.addMutation(mutation);
	}

	protected String createRowId(Audit audit) {
		return idGenerator.generate(audit);
	}

}
