/**
 * 
 */
package com.df.tc.audit;

import java.util.Set;

/**
 * Store audits into accumulo
 * 
 * @author Steve Chernyak
 */
public interface AuditIngester {

	public static final String DEFAULT_TABLE_NAME = "df_audit";

	/**
	 * 
	 * @param audit
	 * @throws AuditException
	 */
	public void ingest(Audit audit) throws AuditException;

	/**
	 * 
	 * @param audits
	 * @throws AuditException
	 */
	public void ingest(Set<Audit> audits) throws AuditException;
}
