/**
 * 
 */
package com.df.tc.audit;

/**
 * @author Steve Chernyak
 * 
 */
public class AuditException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8771536131175278962L;

	public AuditException() {
		super();
	}

	public AuditException(String arg0, Throwable arg1, boolean arg2,
			boolean arg3) {
		super(arg0, arg1, arg2, arg3);
	}

	public AuditException(String arg0, Throwable arg1) {
		super(arg0, arg1);
	}

	public AuditException(String arg0) {
		super(arg0);
	}

	public AuditException(Throwable arg0) {
		super(arg0);
	}

}
