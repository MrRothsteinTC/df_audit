/**
 * 
 */
package com.df.tc.audit;

import java.io.InputStream;
import java.util.Date;

/**
 * @author Steve Chernyak
 * 
 */
public class Audit {

	private Date date;
	private String type;
	private InputStream content;

	public Audit(Date date, String type, InputStream content) {
		this.date = date;
		this.type = type;
		this.content = content;
	}

	public Date getDate() {
		return date;
	}

	public String getType() {
		return type;
	}

	public InputStream getContent() {
		return content;
	}

}
